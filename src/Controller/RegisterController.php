<?php
// src/Controller/RegisterController.php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class RegisterController
{
  public function register(Request $request, SessionInterface $session)
  {
    $response = new Response();
    if ($request->isMethod('POST'))
    {
      $username = $request->request->get('username');
      $first_name = $request->request->get('first_name');
      $last_name = $request->request->get('last_name');
      $response->setContent(json_encode(["username"=>$username, "first_name"=>$first_name, "last_name"=>$last_name]));
      $response->setStatusCode(200);
      return $response;
    }
    $response->setContent("404 not found");
    $response->setStatusCode(404);
    return $response;
  }
}