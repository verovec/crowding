<?php
// src/Controller/LoginController.php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class LoginController
{
  public function login()
  {
    $content = "Login !";
    return new Response($content);
  }
}